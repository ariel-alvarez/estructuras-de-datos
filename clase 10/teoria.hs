-- Interfaz : Nombre de las peraciones | Tipo de las operaciones | Especificación de comportamiento

-- Los constructores data nos dan a entender que estamos tratando con un tipo algebraico
-- Cuando usamos class, es muy probable que no estemos tratando con un tipo algebraico, justamente por eso necesitamos definirle su interfaz


-- QueueInterface.hs
module QueueInterface where
  class Queue q where
    emptyQ   :: q
    queueQ   :: q -> a -> q
    isEmptyQ :: q -> Bool
    firstQ   :: q -> a
    dequeueQ :: q -> q
    


-- Banco.hs
module Banco where
  import QueueInterface
  
  lenQ :: Queue cola => cola -> Int
  lenQ q = if isEmptyQ q
             then 0
             else 1 + lenQ(dequeueQ q)


-- QueueImpl
module QueueImpl where
  import QueueInterface
  data QueueList1 a = Q1[a]
  instance Queue QueueList1 where
    emptyQ = Q1 []
    queueQ (Q1 xs) x = Q1(xs++[x])
    isEmptyQ(Q1 xs) = null xs
    firstQ (Q1 xs) = head xs
    dequeueQ (Q1 xs) = Q1 (tail xs)