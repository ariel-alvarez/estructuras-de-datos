module AbstractList where

class AbstractList ls where
    isEmptyL :: ls a -> Bool
    consL :: a -> ls a -> ls a    
    tailL :: ls a -> ls a
    headL :: ls a -> a
    emptyL :: ls a
    lengthL :: ls a -> Int