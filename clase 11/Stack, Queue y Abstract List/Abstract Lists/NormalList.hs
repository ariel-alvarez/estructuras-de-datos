module NormalList(NormalList) where

import AbstractList

data NormalList a = MkNormalL [a] deriving Show

isEmpty' [] = True
isEmpty' _  = False

instance AbstractList NormalList where
    emptyL = MkNormalL []

    isEmptyL (MkNormalL xs) = isEmpty' xs
    
    consL x (MkNormalL xs) = MkNormalL (x : xs)
    
    tailL (MkNormalL xs) = MkNormalL (tail xs)
    
    headL (MkNormalL xs) = head xs
    
    lengthL (MkNormalL xs) = length xs