module TreeList where

import AbstractList

data Tree a = EmptyT | NodeT a (Tree a) (Tree a) deriving (Show)

data TreeList a = MkListT (Tree a) deriving (Show)

isEmptyT EmptyT = True
isEmptyT _      = False

root (NodeT x _ _) = x

agregarDerecha x t = NodeT x EmptyT t

extrearDerecha (NodeT _ _ t2) = t2

sizeT EmptyT = 0
sizeT (NodeT x _ t2) = 1 + sizeT t2

instance AbstractList TreeList where
    isEmptyL (MkListT t) = isEmptyT t
    
    consL x (MkListT t) = MkListT (agregarDerecha x t)
    
    tailL (MkListT t) = MkListT (extrearDerecha t)
	
    headL (MkListT t) = root t
    
    emptyL = MkListT EmptyT
    
    lengthL (MkListT t) = sizeT t
    