import AbstractList
import NormalList
import TreeList

unaListaN :: NormalList Int
unaListaN = consL 2 (consL 3 emptyL)

unaListaT :: TreeList Int
unaListaT = consL 2 (consL 3 emptyL)












lengthL' :: NormalList Int -> Int
lengthL' xs | isEmptyL xs = 0
            | otherwise   = 1 + lengthL (tailL xs)