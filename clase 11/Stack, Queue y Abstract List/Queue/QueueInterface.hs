module QueueInterface where

class Queue q where
	emptyQ   :: q a
	queueQ   :: q a -> a -> q a
	dequeueQ :: q a -> q a
	firstQ   :: q a -> a
	isEmptyQ :: q a -> Bool