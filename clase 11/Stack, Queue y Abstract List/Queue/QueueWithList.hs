module QueueWithList(QueueWithList) where

import QueueInterface

data QueueWithList a = MkListQ [a]

isEmpty [] = True
isEmpty _  = False

-- Agrego por adelante y saco por atr�s
instance Queue QueueWithList where
    emptyQ                = MkListQ []
    queueQ (MkListQ xs) x = MkListQ (x:xs)
    dequeueQ (MkListQ xs) = MkListQ (tail xs)
    firstQ (MkListQ xs)   = last xs
    isEmptyQ (MkListQ xs) = isEmpty xs