import QueueInterface
import QueueWithList

unaQueue :: QueueWithList Int
unaQueue = queueQ (queueQ emptyQ 3) 2

contarElementos :: QueueWithList Int -> Int
contarElementos q | isEmptyQ q = 0
                  | otherwise  = 1 + 
                                 contarElementos (dequeueQ q)

