module StackInterfaceConMaximo where

class Stack s where
    emptyS   :: s a
    pushS    :: a -> s a -> s a
    popS     :: s a -> s a
    topS     :: s a -> a
    isEmptyS :: s a -> Bool
    lenS     :: s a -> Int
    maxS     :: Ord a => s a -> a -- ponemos Ord para poder usar >, <, >= y <=
    