module StackListConMaximo where

import StackInterfaceConMaximo

-- ¡¡IMPORTANTE!!
-- Inv. Rep.
-- 1) El número que guardamos en la stack es igual
--    al largo de la lista
-- 2) El primero de la lista es el maximo hasta ahora
-- 3) El largo de la lista de maximos es igual al
--    largo de la lista elementos

-- Ejemplos:
-- Pila      Len    Maximos en cada momento
-- []        0      []
-- [1]       1      [1]
-- [2,1]     2      [2,1]
-- [1,2,1]   3      [2,2,1]
-- [3,1,2,1] 4      [3,2,2,1]

-- Vamos quitando para ilustrar un par de casos mas
-- [3,1,2,1] 4  [3,2,2,1]
-- [1,2,1]   3  [2,2,1]
-- [2,1]     2  [2,1]

--                                Lista Len  Maximos
data StackListConMaximo a = MkSLM [a]   Int  [a]

-- Implementar las funciones teniendo en cuenta los Inv. Rep.
-- Cambiar los 'undefined' por la implementación correcta
instance Stack StackListConMaximo where
    emptyS                   = undefined
    pushS x  (MkSLM xs n m)  = undefined
    popS     (MkSLM xs n m)  = undefined
    topS     (MkSLM xs n m)  = undefined
    isEmptyS (MkSLM xs n m)  = undefined
    lenS     (MkSLM xs n m)  = undefined
    maxS     (MkSLM xs n m)  = undefined