module StackList where

import StackInterface

data StackList a = MkSL [a]

instance Stack StackList where
    emptyS              = MkSL []
    pushS x (MkSL xs)   = MkSL (x:xs)
    popS (MkSL xs)      = MkSL (tail xs)
    topS (MkSL xs)      = head xs
    isEmptyS (MkSL xs)  = null xs
    lenS (MkSL xs)      = length xs -- recorre todos los elementos