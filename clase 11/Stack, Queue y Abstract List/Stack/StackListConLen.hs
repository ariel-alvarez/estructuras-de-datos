module StackList where

import StackInterface

-- Inv. Rep.
-- 1) El número que guardamos es igual al largo de la lista

data StackList a = MkSL [a] Int

instance Stack StackList where
    emptyS = MkSL [] 0
    pushS x (MkSL xs n)   = MkSL (x:xs) (n+1)
    popS (MkSL xs n)      = MkSL (tail xs) (n-1)
    topS (MkSL xs n)      = head xs
    isEmptyS (MkSL xs n)  = n == 0
    lenS (MkSL xs n)      = n -- no recorre nada