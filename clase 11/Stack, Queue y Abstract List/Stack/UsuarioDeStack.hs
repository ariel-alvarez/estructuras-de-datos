import StackInterface
import StackList

lensComoUsuario :: StackList a -> Int
lensComoUsuario s | isEmptyS s = 0
                  | otherwise  = 1 + lensComoUsuario (popS s)