--Entre los paréntesis van las cosas que puedan importarse
module SetListNoReps() where

import SetInterFace
data SetListNoReps = SLNR [Int]

instance Set SetListNoReps where
  empty = SLNR []
  
    