-- Viernes 25 de Abril de 2014
-- Ariel Alvarez
--
-- Implementar las funciones dadas de Map utilizando árboles con invariante de BST

class Map m where
    emptyM :: m k v
    lookUpM :: (Eq k, Ord k) => m k v -> k -> Maybe v
    addM :: (Eq k, Ord k) => m k v -> k -> v -> m k v
    removeM :: (Eq k, Ord k) => m k v -> k -> m k v
    domM :: m k v -> [k]

------------------------------------------------------------
------------------------------------------------------------

data BSTree a = EmptyBSTree | NodeBST a (BSTree a) (BSTree a) deriving (Show)

------------------------------------------------------------
------------------------------------------------------------

data BSTMap k v = EmptyBSTMap | MkBSTMap (BSTree (k,v)) deriving (Show)

instance Map BSTMap where

    emptyM = EmptyBSTMap 

    lookUpM (MkBSTMap t) key = lookUpBSTree t key where 
    
        lookUpBSTree EmptyBSTree _ = Nothing
        
        lookUpBSTree (NodeBST (key, val) l r) x
          | key == x = Just(val)
          | key < x = lookUpBSTree r x
          | key > x = lookUpBSTree l x
          
    addM (MkBSTMap t) key val = MkBSTMap (addBST t (key, val)) where
        addBST EmptyBSTree par = NodeBST par EmptyBSTree EmptyBSTree
        addBST (NodeBST h@(nk, nv) l r) par@(xk, xv)
          | nk == xk = NodeBST (xk, xv) l r
          | nk <  xk = NodeBST h l (addBST r par)
          | nk >  xk = NodeBST h (addBST l par) r

      
------------------------------------------------------------
------------------------------------------------------------
