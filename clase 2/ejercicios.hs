-- COLORES --
data Color = Azul | Amarillo | Rojo deriving (Show)

------------
-- TUPLAS --
------------

first:: (a,b) -> a
first (x,_) = x

second:: (a,b) -> b
second (_,y) = y

max' :: (Int, Int) -> Int
max' (x,y) = if x > y then x else y

------------
-- LISTAS --
------------

null' ::  [a] -> Bool
null' [] = True
null' (x:xs) = False

-- pre: not (null xs)
head' :: [a] -> a
head' [] = undefined
head' (x:_) = x

length':: [a] -> Int
length' ([]) = 0
length' ([x]) = 1
length' (x:xs) = 1 + length xs

-- pre: n <= len
drop' :: Int -> [a] -> [a]
drop' 0 xs = xs
drop' n (_:xs) = drop' (n-1) xs

-- pre: n <= len
take' :: Int -> [a] -> [a]
take' _ [] = []
take' 0 xs = []
take' n (x:xs) = x:(take' (n-1) xs)

-- pre: n <= len
split' :: Int -> [a] -> ([a],[a])
split' n xs = (take' n xs, drop' n xs)

append' :: [a] -> [a] -> [a]
append' [] ys = ys
append' (x:xs) ys = x:(append' xs ys )

snoc' :: [a] -> a -> [a]
snoc' xs z = append' xs [z]

reverse' :: [a] -> [a]
reverse' [] = []
reverse' (x:xs) = snoc' (reverse' xs) x

-- pre: not (null xs)
last' :: [a] -> a
last' [x] = x
last' (_:xs) = last' xs
last'' xs = head (reverse xs)
last''' xs = head (drop (length' xs -1) xs)

-----------
-- TalVez --
-----------
data TalVez a = Simplemente a | Nada deriving (Show)
--data TalVez a = Simplemente a | Nada
headM :: [a] -> TalVez a
headM [] = Nada
headM (x:_) = Simplemente x

-- hacer el tail

-- pre: len xs = len ys
zip' :: [a] -> [b] -> [(a,b)]
zip' _ [] = []
zip' [] _ = []
zip' (x:xs) (y:ys) = (x,y):zip' xs ys

-- HACER:
--maximum ::  [Int] -> Int
--flatten :: [[a]] -> [a]

prepend :: TalVez a -> TalVez [a] -> TalVez [a]
prepend Nada _ = Nada
prepend _ Nada = Nada
prepend (Simplemente x) (Simplemente xs) = Simplemente (x:xs)

take'' :: Int -> [a] -> TalVez [a]
take'' _ [] = Nada
take'' 0 xs = Simplemente []
take'' n (x:xs) = prepend (Simplemente x) (take'' (n-1) xs)


--tupla :: TalVez a -> TalVez a -> TalVez (a, a)
--tupla _ Nada = Nada
--tupla Nada _ = Nada
--tupla x y = Simplemente (x,y)







