data Color = Rojo | Azul | Verde | Negro | Blanco deriving (Eq, Show, Enum)
data Persona = MkPersona Int String deriving (Show)

cumplirAnios :: Persona -> Persona
cumplirAnios (MkPersona e n) = MkPersona (e+1) n

cambiarNombre (MkPersona e _) nuevoNombre = MkPersona e nuevoNombre 

data Celda = MkCelda {
    nroAzules :: Int,
    nroRojas :: Int,
    nroVerdes :: Int,
    nroNegras :: Int
} deriving (Show)

type Fila = [Celda]
celdaVacia = MkCelda 0 0 0 0
filaVacia = replicate 4 celdaVacia

newtype Pair a b = Pair (a,b)  deriving (Eq,Show) 

instance Num Color where
    (+) Blanco x = x
    (+) x Blanco = x
    (+) Negro _ = Negro
    (+) _ Negro = Negro
    (+) x y = Verde
    (-) x y = Verde
    (*) x y = Verde
    abs    x = Verde
    signum x = Verde
    fromInteger i = Verde

--instance Int Color where
--    add Verde Azul = Rojo

ponerRoja :: Celda -> Celda
ponerRoja (MkCelda a _ v n)  = MkCelda a 1 v n

lemming :: (a -> b) -> [a] -> [b]  
lemming _ [] = []
lemming f [x] = [f x]
lemming f (x:xs) = (f x):(lemming f xs) 

ponerRojaEnCadaUna :: Fila -> Fila
ponerRojaEnCadaUna fila = lemming ponerRoja fila