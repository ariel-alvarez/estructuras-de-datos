-- Repaso Maybe
dropM :: Int -> [a] -> Maybe [a]
dropM n xs | n > (length xs) = Nothing
dropM n xs = Just (drop n xs)

-- Colores : )
data Color = Azul | Rojo | Verde | Negro deriving (Eq, Show)
type Fila = [Celda]

data Celda = MkCelda {
    nroAzules :: Int,
    nroRojas :: Int,
    nroVerdes :: Int,
    nroNegras :: Int
} deriving (Show)

celdaVacia = MkCelda 0 0 0 0
filaVacia = replicate 4 celdaVacia

type Tablero = [Fila]

filaDe n = if n == 0 then [] else celdaVacia : filaDe (n-1)
tableroDe :: Int -> Int -> Tablero
tableroDe 0 c = []
tableroDe fila columna = filaDe columna : tableroDe (fila-1) columna

bolitasEnCelda :: Celda -> Int
bolitasEnCelda (MkCelda a r v n) = a+r+v+n

bolitasEnFila :: Fila -> Int
bolitasEnFila [] = 0
bolitasEnFila (c:cs) = bolitasEnCelda c + bolitasEnFila cs

bolitasEnTablero :: Tablero -> Int
bolitasEnTablero [] = 0
bolitasEnTablero (r:rs) = bolitasEnFila r + bolitasEnTablero rs


fusionarCelda:: Celda -> Celda -> Celda
fusionarCelda (MkCelda x1 x2 x3 x4) (MkCelda y1 y2 y3 y4) = MkCelda (x1+y1) (x2+y2) (x3+y3) (x4+y4)

fusionarFila :: Fila -> Fila -> Fila
fusionarFila [] [] = []
fusionarFila (celda1:fila1) (celda2:fila2) = fusionarCelda celda1 celda2 : (fusionarFila fila1 fila2)

fusionarTablero :: Tablero -> Tablero -> Tablero
fusionarTablero [] [] = []
fusionarTablero (fila1:t1s) (fila2:t2s) = fusionarFila fila1 fila2 : (fusionarTablero t1s t2s)

-- esto es un zipWith
fusionar _ [] [] = []
fusionar subf (x:xs) (y:ys) = subf x y : (fusionar subf xs ys)

fusionarFila' fila1 fila2 = fusionar fusionarCelda fila1 fila2
fusionarTablero' t1 t2 = fusionar fusionarFila' t1 t2

-- Dada una lista de tableros -> el tablero con más bolitas

maximo :: [Int] -> Int
maximoComparar x [] = x
maximoComparar x [y] | x > y = y
                     | otherwise = y
maximo (x:xs) = maximoComparar x xs

--tableroConMasBolitas tableros = max.map bolitasEnTablero tableros






