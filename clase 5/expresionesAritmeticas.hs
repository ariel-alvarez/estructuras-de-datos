data ExpA = Suma ExpA ExpA | Mult ExpA ExpA | Nro Int deriving(Show)

e1 :: ExpA
e1 = Suma (Mult (Nro 4) (Nro 2)) (Mult (Nro 5) (Nro 3))


e2 :: ExpA
e2 = Mult (Nro 2) (Suma e1 (Nro (-2)))

e3 = Suma (Mult (Nro 1) (Nro 2)) (Nro 0)

eval :: ExpA -> Int
eval (Nro n) = n
eval (Suma e1 e2) = eval e1 + (eval e2)
eval (Mult e1 e2) = eval e1 * (eval e2)


simpleEA :: ExpA -> ExpA

simpleEA (Nro x) = Nro x

simpleEA (Suma x (Nro 0)) = simpleEA x
simpleEA (Suma (Nro 0) y) = simpleEA y

simpleEA (Mult x (Nro 1)) = simpleEA x
simpleEA (Mult (Nro 1) y) = simpleEA y
