-- preorder   Root Izq  Der
-- inorder    Izq  Root Der
-- postorder  Izq  Der  Root

--levels Tree a -> [[a]]
--levels (Node root izq der) = 
--
-- Anti Thrust
--
--[[1], [2, [4,5] ,3, [6,7]]

data Poli = Cte Int | Var | Add Poli Poli | Mul Poli Poli

eval :: Poli -> Int -> Int
eval (Cte k) n   = k
eval Var       n = n
eval (Add p q) n = (eval p n) + (eval q n)
eval (Mul p q) n = (eval p n) * (eval q n)

add :: Poli -> Poli -> Poli
add p q = Add p q

adds :: Poli -> Int
adds (Add p q) = 1 + adds p + adds q
adds (Mul p q) = add p + add q
adds _ = 0

mul :: Int -> Poli -> Poli
mul n p = Mul (Cte n) p

data ConstantNames = X deriving(Show)
   
instance Show Poli where
   show Var = show X
   show (Cte c) = show c
   show (Add p q) = (show p) ++ "+" ++ (show q) 
   show (Mul p q) = (show p) ++ "." ++ (show q) 
   
distribute :: Poli -> Poli
distribute (Cte k) = Cte k
distribute Var = Var
distribute (Add p q) = Add (distribute p) (distribute q)
distribute (Mul p q) = distribute p q
    where distributeMul p (Add p' q') = Add (distribute (Mul p p')) ( distribute (Mul p q'))
    where distributeMul (Add p' q') p = Add (distribute (Mul p' p)) ( distribute (Mul q' p))
    where distributeMul p q = if adds == 0 && adds q == 0 then 
