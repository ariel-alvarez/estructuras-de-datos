data Exp = Const Int | Var String | UnExp UnOp Exp | BinExp BinOp Exp Exp
data UnOp = Neg
data BinOp = Add | Sub | Mul | Div
type Env = [(String,Int)]


class Set S where
  empty :: S
belongs :: S -> Int -> Bool
add :: S -> Int -> S
remove :: S -> Int-> S
size :: S -> Int



-- Hacer el 
-- data SetListSorteredSize = SLSS [Int] Int